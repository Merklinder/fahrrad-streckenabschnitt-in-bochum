# Fahrrad-Streckenabschnitt in Bochum (Bövinghauser Hellweg)

## Historie

* 2020-09-29 - E-Mail an Herrn Schmittem
* 2020-10-16 - Erinnerung an Herrn Schmittem
* 2020-10-19 - Antwort von Herrn Schmittem
* 2021-02-16 - Wiedervorlage per Mail an Hr. Schmittem

## zukünftiger Radweg von Bochum Gerthe nach Castrop-Rauxel parallel zum Bövinghauser Hellweg

* [Thema Beleuchtung](https://session.bochum.de/bi/vo0050.asp?__kvonr=7078518)
